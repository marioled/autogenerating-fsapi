# Oracle types to java

## How to run application

1. clone repository


2. set program arguments:
    - password: '--spring.datasource.password=&lt;password&gt;'
    - username: '--spring.datasource.username=&lt;username&gt;' (optional, default=fs)
    - database url: '--spring.datasource.url=&lt;url&gt;' (default fsuv)
    - web server port: '--server.port=&lt;port&gt;' (default 9000)


3. Start web server
    - from IntelliJ IDE or
    - build java jar file (maven package) and run java jar file with script or full command line
        - start_web_server.sh
        - java -jar target/generate-java.jar --spring.datasource.password=<password>
          --spring.datasource.username=<username>
          --spring.datasource.url=<url>


4. in browser open
    - list of all types:
      http://localhost:9000/schemas/{schema}/types
    - prints implementation of java business entity for specified type
      http://localhost:9000/schemas/{schema}/types/{type}


5. run app in console command line (see script generate_cli.sh for usage)
    - generate_cli.sh
    - java -jar target/generate-java.jar --spring.main.web-application-type=NONE --spring.datasource.password=<password>
      --spring.datasource.username=<username> --spring.datasource.url=<url> --type=<oracle_type> --type=<oracle_type>
      ...


6. Optional parameters:
   --schema=<schema> (seach oracle type in <schema>; default schema 'FS')
   --generator.path=<path> (save files to location <path>; default is current folder)
   --recursion.level=1     (save parent and first level oracle subtypes)
   --recursion.level=all   (save parent and all level oracle subtypes)
   