package no.unit.oracletypestojavapojo;

import no.unit.oracletypestojavapojo.oracletype.OracleTypeController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.stream.Stream;

@SpringBootApplication
public class AutogeneratingJavaFilesApplication implements CommandLineRunner {

    public static final String CURRENT_FOLDER = ".";

    @Autowired
    private OracleTypeController controler;

    public static void main(String[] args) {
        SpringApplication.run(AutogeneratingJavaFilesApplication.class, args);
    }

    @Override
    public void run(String... args) {

        if (Stream.of(args).noneMatch(arg -> arg.startsWith("--type"))) {
            displayMessage();
            return;
        }

        if (Stream.of(args).noneMatch(arg -> arg.startsWith("--spring.datasource.password"))) {
            displayMessage();
            return;
        }

        String schema = Stream.of(args)
                .filter(arg -> arg.startsWith("--schema"))
                .map(arg -> arg.split("=")[1])
                .findFirst()
                .orElse("FS");

        String recursion = Stream.of(args)
                .filter(arg -> arg.startsWith("--recursion"))
                .map(arg -> arg.split("=")[1])
                .findFirst()
                .orElse("none");

        String path = Stream.of(args)
                .filter(arg -> arg.startsWith("--generator.path"))
                .map(arg -> arg.split("=")[1])
                .findFirst()
                .orElse(CURRENT_FOLDER);

        Stream.of(args)
                .filter(a -> a.startsWith("--type"))
                .map(arg -> arg.split("=")[1])
                .map(String::toUpperCase)
                .forEach(type -> {
                    switch (recursion) {
                        case "none":
                            controler.saveToJavaPojo(schema, type, path);
                            break;
                        case "1":
                            controler.saveToJavaPojoOneLevel(schema, type, path);
                            break;
                        case "all":
                            controler.saveToJavaPojoAllLevels(schema, type, path);
                            break;
                        default:
                            System.out.println("Unknown recursive level " + recursion);
                            displayMessage();
                    }
                });
    }

    private static void displayMessage() {
        System.out.println("java -jar target/generate-java.jar --spring.main.web-application-type=NONE --spring.datasource.password=<password> --type=<oracle_type> --type=<oracle_type> --type=<oracle_type> ...");
        System.out.println();
        System.out.println("Optional parameters: --schema=<schema> (search oracle type in <schema>; default schema 'FS')");
        System.out.println("                     --generator.path=<path> (save files to location <path>; default is current folder)");
        System.out.println("                     --recursion.level=1     (save parent and first level oracle subtypes)");
        System.out.println("                     --recursion.level=all   (save parent and all level oracle subtypes)");

    }
}
