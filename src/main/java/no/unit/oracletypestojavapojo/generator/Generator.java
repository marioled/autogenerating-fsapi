package no.unit.oracletypestojavapojo.generator;

import no.unit.oracletypestojavapojo.templates.JavaTemplate;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class Generator {

    public void save(JavaTemplate template, String path) {
        Path filepath = Paths.get(path, template.toFilename());
        try (BufferedWriter writer = Files.newBufferedWriter(filepath, StandardCharsets.UTF_8)) {
            String content = template.toJava();
            writer.write(content);
            System.out.println("=================================================");
            System.out.println(content);
            System.out.println("=================================================");
            System.out.println("File saved " + filepath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}