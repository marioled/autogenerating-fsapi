package no.unit.oracletypestojavapojo.oracletype;

import no.unit.oracletypestojavapojo.oracletype.model.OracleType;
import no.unit.oracletypestojavapojo.templates.*;

public class JavaTemplateFactory {
    public static JavaTemplate getTemplate(OracleType oracleType) {
        JavaTemplate template = null;

        if (oracleType.isId()) {
            template = new JavaIdTemplate();
        } else if (oracleType.isModel()) {
            template = new JavaModelTemplate();
        } else if (oracleType.isFilter()) {
            template = new JavaFilterTemplate();
        } else if (oracleType.isInfo()) {
            template = new JavaInfoTemplate();
        } else {
            template = new JavaInfoTemplate();
        }

        if (template != null) {
            template.setOracleType(oracleType);
        }

        return template;
    }
}
