package no.unit.oracletypestojavapojo.oracletype;

import lombok.extern.java.Log;
import no.unit.oracletypestojavapojo.generator.Generator;
import no.unit.oracletypestojavapojo.oracletype.model.OracleType;
import no.unit.oracletypestojavapojo.oracletype.model.OracleTypeAttribute;
import no.unit.oracletypestojavapojo.templates.JavaTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Controller
@Log
public class OracleTypeController {

    @Autowired
    private OracleTypeRepository repository;

    @Autowired
    private Generator generator;

    @GetMapping("/schemas/{schema}/types")
    public String showAllOracleTypes(Model model, @PathVariable String schema) {
        List<String> oracleTypes = repository.findAll(schema);
        Map<String, List<String>> oracleTypesGrouped;
        oracleTypesGrouped = oracleTypes.stream()
                .collect(groupingBy(name -> name.substring(0,1)));
        model.addAttribute("types", oracleTypesGrouped);
        model.addAttribute("schema", schema);
        return "oracleTypes";
    }

    @GetMapping("/schemas/{schema}/types/{oracleTypeName}")
    public String saveToJavaPojoModel(
            Model model,
            @PathVariable String schema,
            @PathVariable String oracleTypeName) {
        OracleType oracleType = repository.findByName(schema, oracleTypeName);
        JavaTemplate template = JavaTemplateFactory.getTemplate(oracleType);
        model.addAttribute("template", template);
        return "java";
    }

    public JavaTemplate saveToJavaPojo(String schema, String name, String path) {
        OracleType oracleType = repository.findByName(schema, name);
        JavaTemplate template = JavaTemplateFactory.getTemplate(oracleType);
        generator.save(template, path);
        return template;
    }

    public JavaTemplate saveToJavaPojoOneLevel(String schema, String name, String path) {
        JavaTemplate template = saveToJavaPojo(schema, name, path);
        template.getOracleType()
                .getAttributes()
                .stream()
                .filter(OracleTypeAttribute::isPrimitive)
                .map(OracleTypeAttribute::getElementType)
                .forEach(element -> saveToJavaPojo(schema, element, path));
        return template;
    }

    public JavaTemplate saveToJavaPojoAllLevels(String schema, String name, String path) {
        JavaTemplate template = saveToJavaPojo(schema, name, path);
        template.getOracleType()
                .getAttributes()
                .stream()
                .filter(OracleTypeAttribute::isPrimitive)
                .map(OracleTypeAttribute::getElementType)
                .forEach(element -> saveToJavaPojoAllLevels(schema, element, path));
        return template;
    }
}
