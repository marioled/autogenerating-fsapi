package no.unit.oracletypestojavapojo.oracletype;

import no.unit.oracletypestojavapojo.oracletype.model.OracleType;
import no.unit.oracletypestojavapojo.oracletype.model.OracleTypeAttribute;
import no.unit.oracletypestojavapojo.oracletype.model.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OracleTypeRepository {
    
    private static final String SQL = String.join(" ",
            "SELECT A.attr_name                             name",
            "     , NVL(T.typecode, 'PRIMITIVE')            wrapper",
            "     , A.attr_type_name                        type",
            "     , NVL(C.elem_type_name, A.attr_type_name) subtype",
            "     , A.length                                length",
            "     , A.precision                             precision",
            "     , A.scale                                 scale",
            "  FROM      SYS.all_type_attrs  A",
            "  LEFT JOIN SYS.all_types       T on     A.attr_type_name = T.type_name",
            "                                     AND A.OWNER = T.OWNER",
            "  LEFT JOIN SYS.all_coll_types  C on     A.attr_type_name = C.type_name",
            "                                     AND A.OWNER = C.OWNER",
            " WHERE A.owner = ?",
            "   AND A.type_name = ?",
            " ORDER BY A.attr_no ASC"
    );

    private static final String SQL_TYPES = String.join(" ",
            "SELECT T.type_name",
            "  FROM SYS.all_types       T",
            " WHERE T.owner = ?",
            "   AND T.typecode = 'OBJECT'",
            " ORDER BY T.type_name"
    );

    private final JdbcTemplate jdbc;

    @Autowired
    public OracleTypeRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public List<String> findAll(String schema) {
        return jdbc.query(SQL_TYPES, (rs, i) -> rs.getString("type_name"), schema);
    }

    public OracleType findByName(String schema, String name) {
        List<OracleTypeAttribute> attributes = jdbc.query(SQL, (rs, row) -> {
            String attributeName = rs.getString("name");
            Wrapper wrapper = Wrapper.valueOf(rs.getString("wrapper"));
            String type = rs.getString("type");
            String subtype = rs.getString("subtype");
            OracleTypeAttribute attribute = new OracleTypeAttribute(attributeName, type, wrapper, subtype);
            if (wrapper == Wrapper.PRIMITIVE) {
                attribute.setLength(rs.getInt("length"));
                attribute.setPrecision(rs.getInt("precision"));
                attribute.setScale(rs.getInt("scale"));
            }
            return attribute;
        }, schema, name);

        return new OracleType(name, attributes);
    }
}
