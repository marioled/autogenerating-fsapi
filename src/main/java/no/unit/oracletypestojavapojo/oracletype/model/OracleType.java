package no.unit.oracletypestojavapojo.oracletype.model;

import com.google.common.base.CaseFormat;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class OracleType {

    @NonNull
    private String name;

    @NonNull
    private List<OracleTypeAttribute> attributes;

    public String toJavaClass(String suffix) {
        String javaClassName;
        if (name.endsWith(suffix)) {
            javaClassName = name.substring(0, name.lastIndexOf(suffix));
        } else {
            javaClassName = name;
        }
        if (javaClassName.endsWith("1_ID")) {
            javaClassName = javaClassName.replaceAll("1_ID", "_ID");
        }
        if (javaClassName.endsWith("1")) {
            javaClassName = javaClassName.substring(0, javaClassName.length() - 1);
        }
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, javaClassName);
    }

    public boolean isModel() {
        return !(isId() || isFilter() || isInfo())
                && hasId();
    }

    public boolean isId() {
        return name.contains("_ID_");
    }
    public boolean isFilter() {
        return name.contains("_FILTER_");
    }

    public boolean isInfo() {
        return name.contains("_INFO_");
    }

    public boolean hasId() {
        return getAttributes().stream()
                .anyMatch(attr -> attr.is("id"));
    }

    public String getIdType() {
        return getAttributes().stream()
                .filter(attr -> attr.is("id"))
                .map(OracleTypeAttribute::toJavaType)
                .findFirst()
                .orElse(null);
    }

    public String getName() {
        return name;
    }
}
