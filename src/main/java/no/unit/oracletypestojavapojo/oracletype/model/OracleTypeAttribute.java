package no.unit.oracletypestojavapojo.oracletype.model;

import com.google.common.base.CaseFormat;
import lombok.Data;
import lombok.NonNull;
import no.unit.oracletypestojavapojo.templates.JavaTemplate;

@Data
public class OracleTypeAttribute {

    @NonNull
    private String name;

    @NonNull
    private String type;

    @NonNull
    private Wrapper wrapper; // COLLECTION, OBJECT, PRIMITIVE

    @NonNull
    private String elementType;

    private int length;

    private int precision;

    private int scale;

    public boolean isObject() {
        return wrapper == Wrapper.OBJECT;
    }

    public boolean isCollection() {
        return wrapper == Wrapper.COLLECTION;
    }

    public boolean isPrimitive() {
        return wrapper == Wrapper.PRIMITIVE;
    }

    public boolean is(String fieldName) {
        return fieldName.equalsIgnoreCase(this.getName());
    }

    public boolean isNot(String fieldName) {
        return !is(fieldName);
    }

    public String toJavaType() {
        return toJavaType("_T");
    }

    public String toJavaType(String suffix) {
        if (isCollection()) {
            return collectionOf(toJavaClass(suffix));
        }
        if (isObject()) {
            return toJavaClass(suffix);
        }
        if (isPrimitive()) {
            return getJavaPrimitive();
        }
        throw new RuntimeException("Invalid oracle attribute " + this);
    }

    private String collectionOf(String javaClassName) {
        return "Collection<" + javaClassName + ">";
    }

    private String getJavaPrimitive() {
        switch (elementType) {
            case "NUMBER":
                if (scale > 0) {
                    return "BigDecimal";
                }
                if (0 < precision && precision < 9) {
                    return "Integer";
                }
                if (9 <= precision && precision < 18) {
                    return "Long";
                }
                return "BigInteger";
            case "VARCHAR2":
            case "CLOB":
                return "String";
            case "DATE":
                return "Date";
            case "BLOB":
                return "byte[]";
            default:
                return "Unknown";
        }
    }

    public String toJavaClass(String suffix) {
        String javaClassName;
        if (elementType.endsWith(suffix)) {
            javaClassName = elementType.substring(0, elementType.lastIndexOf(suffix));
        } else {
            javaClassName = elementType;
        }
        if (javaClassName.endsWith("1_ID")) {
            javaClassName = javaClassName.replaceAll("1_ID", "_ID");
        }
        if (javaClassName.endsWith("1")) {
            javaClassName = javaClassName.substring(0, javaClassName.length() - 1);
        }
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, javaClassName);
    }

    public String toJavaProperty() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, name);
    }

    public String toJavaDefinition() {
        return toJavaDefinition(JavaTemplate.SUFFIX);
    }

    public String toJavaDefinition(String suffix) {
        return toJavaType(suffix) + " " + toJavaProperty();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Wrapper getWrapper() {
        return wrapper;
    }

    public String getElementType() {
        return elementType;
    }

    public int getLength() {
        return length;
    }

    public int getPrecision() {
        return precision;
    }

    public int getScale() {
        return scale;
    }
}
