package no.unit.oracletypestojavapojo.oracletype.model;

public enum Wrapper {
    COLLECTION, OBJECT, PRIMITIVE
}
