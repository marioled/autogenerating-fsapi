package no.unit.oracletypestojavapojo.templates;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.StringJoiner;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
public class JavaIdTemplate extends JavaTemplate {

    public String toFilename() {
        return toJavaClassName() + ".java";
    }

    @Override
    public String toJava() {
        StringJoiner joiner = new StringJoiner("");
        joiner.add("package <område>." + toJavaClassName().replace("Id", "").toLowerCase() + ";").add(NEWLINE)
                .add(NEWLINE)
                .add("@JdbcBeanSqlName(\"" + getOracleType().getName() + "\")").add(NEWLINE)
                .add("public class " + toJavaClassName() + " extends IdBase" + " {").add(NEWLINE)
                .add(NEWLINE)
                .add(propertiesDefinition())
                .add(NEWLINE)
                .add(TAB).add("public " + toJavaClassName() + "() { }").add(NEWLINE)
                .add(NEWLINE)
                .add(TAB).add("public " + toJavaClassName()).add("(" + fullConstructorParams() + ") {").add(NEWLINE)
                .add(fullConstractorAssigments()).add(NEWLINE)
                .add(TAB).add("}").add(NEWLINE)
                .add(NEWLINE)
                .add(getIdComponents())
                .add(NEWLINE)
                .add(setIdStatementElements())
                .add(NEWLINE)
                .add(idOfComponents())
                .add(NEWLINE)
                .add("}");
        return joiner.toString();
    }

    private String propertiesDefinition() {
        return getOracleType().getAttributes().stream()
                .filter(attr -> attr.isNot("typekode"))
                .map(attr -> {
                    String annotation = "";
                    if (attr.isObject()) {
                        annotation = TAB + "@JdbcBeanStruct" + NEWLINE;
                    }
                    if (attr.isCollection()) {
                        annotation = TAB + "@JdbcBeanArray(arrayName = \"" + attr.getType() + "\")" + NEWLINE;
                    }
                    return annotation + TAB + "private" + " " + attr.toJavaDefinition() + ";" + NEWLINE;
                })
                .collect(Collectors.joining(NEWLINE));
    }

    private String fullConstructorParams() {
        return getOracleType().getAttributes().stream()
                .filter(attr -> attr.isNot("typekode"))
                .map(attr -> attr.toJavaDefinition())
                .collect(Collectors.joining(", "));
    }

    private String fullConstractorAssigments() {
        return getOracleType().getAttributes().stream()
                .filter(attr -> attr.isNot("typekode"))
                .map(attr -> TAB + TAB + "this." + attr.toJavaProperty() + " = " + attr.toJavaProperty())
                .collect(Collectors.joining(NEWLINE));
    }

    private String getIdComponents() {
        String functionDefinition = TAB + "@Override" + NEWLINE +
                TAB + "public void getIdComponents(Collection<String> ids) {" + NEWLINE;
        return functionDefinition +
                getOracleType().getAttributes().stream()
                        .filter(attr -> attr.isNot("typekode"))
                        .map(attr -> TAB + TAB + "Objects.requireNonNull(" + attr.toJavaProperty() + ")")
                        .collect(Collectors.joining(NEWLINE)) +
                NEWLINE + NEWLINE +
                getOracleType().getAttributes().stream()
                        .filter(attr -> attr.isNot("typekode"))
                        .map(attr -> TAB + TAB + (
                                attr.isPrimitive() ?
                                        "ids.add(" + attr.toJavaProperty() + ("NUMBER".equals(attr.getElementType()) ? ".toString());" : ");")
                                        : attr.toJavaProperty() + ".getIdComponents(ids);")
                        )
                        .collect(Collectors.joining(NEWLINE)) +
                NEWLINE +
                TAB + "}" + NEWLINE;
    }

    private String setIdStatementElements() {
        return TAB + "public static void setIdStatementElements(" + toJavaClassName() + " id, List<Object> statementList) {" + NEWLINE +
                getOracleType().getAttributes().stream()
                        .filter(attr -> attr.isNot("typekode"))
                        .map(attr -> TAB + TAB + (
                                attr.isPrimitive() ?
                                        "statementList.add(id != null ? id." + toJavaGetter(attr.toJavaProperty()) + " : null);"
                                        : attr.toJavaType() + ".setIdStatementElements(id != null ? id." + toJavaGetter(attr.toJavaProperty()) + " : null, statementList);")
                        )
                        .collect(Collectors.joining(NEWLINE)) +
                NEWLINE +
                TAB + "}" + NEWLINE;
    }

    private String idOfComponents() {
        return TAB + "public static " + toJavaClassName() + " of(Deque<String> idcomponents) {" + NEWLINE +
                TAB + TAB + "return new " + toJavaClassName() + "(" + NEWLINE +
                getOracleType().getAttributes().stream()
                        .filter(attr -> attr.isNot("typekode"))
                        .map(attr -> TAB + TAB + TAB +
                                (attr.isPrimitive() ?
                                        "NUMBER".equals(attr.getElementType()) ? "Integer.parseInt(idcomponents.removeFirst())" : "idcomponents.removeFirst()"
                                        : attr.toJavaType() + ".of(idcomponents)"))
                        .collect(Collectors.joining("," + NEWLINE)) +
                NEWLINE +
                TAB + TAB + ")" +
                NEWLINE +
                TAB + "}" + NEWLINE;
    }
}

