package no.unit.oracletypestojavapojo.templates;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.StringJoiner;

@EqualsAndHashCode(callSuper = true)
@Data
public class JavaInfoTemplate extends JavaTemplate {

    @Override
    public String toJava() {
        StringJoiner joiner = new StringJoiner(NEWLINE);
        joiner.add("package common;" + NEWLINE)
                .add("@JdbcBeanSqlName(\"" + getOracleType().getName() + "\")")
                .add("public class " + toJavaClassName() + " {" + NEWLINE);

        getOracleType().getAttributes().stream()
                .forEach(attr -> {
                    if (attr.isObject()) {
                        joiner.add(TAB + "@JdbcBeanStruct");
                    }
                    if (attr.isCollection()) {
                        joiner.add(TAB + "@JdbcBeanArray(arrayName = \"" + attr.getType() + "\")");
                    }
                    joiner.add(TAB + "private" + " " + attr.toJavaDefinition() + ";" + NEWLINE);
                });
        joiner.add(TAB + "public " + toJavaClassName() + "() {").add(TAB + "}");
        joiner.add("}");
        return joiner.toString();
    }

    public String toFilename() {
        return toJavaClassName() + ".java";
    }
}
