package no.unit.oracletypestojavapojo.templates;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.StringJoiner;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
public class JavaModelTemplate extends JavaTemplate {

    @Override
    public String toJava() {
        StringJoiner joiner = new StringJoiner("");
        joiner.add("package <område>." + toJavaClassName().toLowerCase() + ";").add(NEWLINE)
                .add(NEWLINE)
                .add("@JdbcBeanSqlName(\"" + getOracleType().getName() + "\")").add(NEWLINE)
                .add("public class " + toJavaClassDefinition() + " {" + NEWLINE)
                .add(NEWLINE)
                .add(propertiesDefinition())
                .add(NEWLINE)
                .add(TAB + "public " + toJavaClassName() + "() { }")
                .add(NEWLINE)
                .add("}");
        return joiner.toString();
    }

    private String toJavaClassDefinition() {
        return toJavaClassName() + " extends Model<" + getOracleType().getIdType() + ">";
    }

    private String propertiesDefinition() {
        return getOracleType().getAttributes().stream()
                .filter(attr -> attr.isNot("id") && attr.isNot("total_antall"))
                .map(attr -> {
                    String annotation = "";
                    if (attr.isObject()) {
                        annotation = TAB + "@JdbcBeanStruct" + NEWLINE;
                    }
                    if (attr.isCollection()) {
                        annotation = TAB + "@JdbcBeanArray(arrayName = \"" + attr.getType() + "\")" + NEWLINE;
                    }
                    return annotation + TAB + "private" + " " + attr.toJavaDefinition() + ";" + NEWLINE;
                })
                .collect(Collectors.joining(NEWLINE));
    }

    public String toFilename() {
        return toJavaClassName() + ".java";
    }
}
