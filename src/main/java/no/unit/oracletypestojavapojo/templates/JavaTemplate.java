package no.unit.oracletypestojavapojo.templates;

import com.google.common.base.Strings;
import com.google.common.html.HtmlEscapers;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import no.unit.oracletypestojavapojo.oracletype.model.OracleType;

@Data
@NoArgsConstructor
public abstract class JavaTemplate {

    public static final int SPACE_LENGTH = 4;
    public static final String TAB = Strings.repeat(" ", SPACE_LENGTH);
    public static final String NEWLINE = "\r\n";
    public static final String SUFFIX = "_T";

    @NonNull
    private OracleType oracleType;

    public abstract String toJava();

    public abstract String toFilename();

    public String toJavaHtml() {
        return HtmlEscapers.htmlEscaper().escape(toJava());
    }

    public String toJavaClassName() {
        return getOracleType().toJavaClass(SUFFIX);
    }

    public String toJavaGetter(String property) {
        return "get" + property.substring(0, 1).toUpperCase() + property.substring(1) + "()";
    }
}
