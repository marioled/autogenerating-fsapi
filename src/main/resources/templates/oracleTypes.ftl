<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
  <body>
    <div class="container">
      <div class="row">
        <#list types as key, values>
          <div class="col-sm-4 my-2">
            <div class="card" style="max-width: 20rem;">
              <div class="card-body">
                <div class="card-title text-center bg-info text-white">${key}</div>
                <p class="card-text">
                  <#list values as value>
                    <a class="small text-secondary" href="/schemas/${schema}/types/${value}">${value}&nbsp;
                    <#if value?contains("_ID_")><span class="badge badge-pill badge-success float-right">id</span>
                    <#elseif value?contains("_FILTER_")><span class="badge badge-pill badge-warning float-right">filter</span>
                    <#elseif value?contains("_INFO_")><span class="badge badge-pill badge-info float-right">info</span>
                    <#else><span class="badge badge-pill badge-primary float-right">model</span></#if></a>
                    <br/>
                  </#list>
                </p>
              </div>
            </div>
          </div>
        </#list>
      </div>
    </div>
  </body>
</html>
