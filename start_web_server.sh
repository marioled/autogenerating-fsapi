#!/bin/bash

path_to_jar="target/generate-java.jar"
username=$FS_USERNAME
password=$FS_PASSWORD
url="${FS_URL:-jdbc:oracle:thin:@localhost:1527/FSUTV.uio.no}"

username_option="--spring.datasource.username=$username"
password_option="--spring.datasource.password=$password"
url_option="--spring.datasource.url=$url"

java -jar "$path_to_jar" "$username_option" "$password_option" "$url_option"
